inline bool Options::cls() const
{
    return d_cls;
}

inline std::string const &Options::expire() const
{
    return d_expire;
}

inline std::string const &Options::ip4() const
{
    return d_ip4;
}

inline bool Options::interactive() const
{
    return d_interactive;
}

inline bool Options::logIgnored(eAction type) const
{
    return d_logIgnored[type];
}


inline std::ostream *Options::logPtr() const
{
    return d_logPtr.get();
}

inline std::ofstream *Options::mailFile(eAction type) const
{
    return d_mailFile[type].get();
}

// static
inline char const *Options::mailType(eAction type)
{
    return s_action[type][1];
}

inline bool Options::preamble() const
{
    return d_preamble;
}

inline std::string const &Options::received() const
{
    return d_received;
}

inline std::ifstream Options::rulesStream()
{
    return std::move(*d_rules.release());
}

inline bool Options::syntax() const
{
    return d_syntax;
}

inline auto Options::beginEndRE(std::string const &target)
{
    return d_arg.beginEndRE(target);
}
