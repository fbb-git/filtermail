#define XERR
#include "options.ih"

//  open the defined mail files, or ignore the not-specified mail files

void Options::mailFiles()
{
    for (unsigned begin = ACCEPT, end = eActionSize; begin != end; ++begin)
    {
        d_logIgnored[begin] = false;
        string name;
        d_arg.option(&name, s_action[begin][0]);

        if (name.empty())
        {
            *d_logPtr << "not saving " << s_action[begin][1] << 
                        " mail on file" << endl;
            continue;
        }

        if (name.find(":HDRS:") == 0)
        {
            d_logIgnored[begin] = true;
            name = name.erase(0, 6);
        }

        setBase(name);

        d_mailFile[begin].reset(                    // open the requested file
            new ofstream{ 
                Exception::factory<ofstream>(
                    name, ios::in | ios::ate, ios::ate
                )
            }
        );
    }        
}
