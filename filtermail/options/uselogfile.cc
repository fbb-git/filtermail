//#define XERR
#include "options.ih"

void Options::useLogFile(string const &spec)
{
    if (                    // verify the the log file's dir. exists
        not create_directories(path{ spec }.parent_path(), s_ec)
        and s_ec.value() != 0
    )
        throw Exception{} << "cannot access " << spec << "'s dir.";

    d_outStreamPtr.reset(   // verify that the log file exists
                        new ofstream{
                            Exception::factory<ofstream>
                            (
                                spec, 
                                ios::in | ios::ate, ios::ate
                            )
                        }
    );

                            // the buffer for *d_logPtr when logging
    d_logBufPtr.reset(new LogBuf{ *d_outStreamPtr });

    d_logPtr.reset(new ostream{ d_logBufPtr.get() });
}





