//#define XERR
#include "options.ih"

Options::Options(void (*usage)(string const &),
                 char const *version, FBB::ArgConfig &arg)
:
    d_arg(arg),
    d_inspect(arg.option('I') != 0),
    d_preamble(arg.option(0, "preamble") != 0),
    d_syntax(arg.option(0, "syntax") != 0)
{
    if (not d_inspect)
    {
        d_arg.versionHelp(usage, version, 1);
    
        d_arg.option(&d_expire, 'e');
        d_interactive = arg.option('i') != 0;
    }

    setBase();
    configFile();                   // cd to the configfile's dir, load it.

    if (inspect())
        return;

    rulesFile();
    logFile();

    if (expiration())
        return;

    mailFiles();

    if (not d_arg.option(&d_ip4, 'p'))
        d_ip4 = s_ip4;
}
