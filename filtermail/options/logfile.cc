//#define XERR
#include "options.ih"

void Options::logFile()
{
    string spec;
    d_arg.option(&spec, 'l');               // maybe initialize logging 

    if (spec.empty())                       // no logging
        return;

    setBase(spec);

    if (spec.front() == '/')    // path to a log file was specified
        useLogFile(spec);
    else
        useSyslog(spec);
}
