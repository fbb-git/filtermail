//#define XERR
#include "options.ih"

string Options::s_accept{ "~/accept" };
char const *Options::s_action[][2] =
{
    { "accept", "standard"       },
    { "ignore", "'ignore' rated" },
    { "spam",   "'spam' rated"   },
};

string Options::s_base{ '~' };
string Options::s_configDir;
string Options::s_ip4 = R"(\[((\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3}))\])";

char const Options::s_configFile[] = "~/.filtermail/config";


error_code Options::s_ec;
unique_ptr<Options> Options::s_optionsPtr;

