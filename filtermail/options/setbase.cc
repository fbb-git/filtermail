//#define XERR
#include "options.ih"

void Options::setBase()
{
    if (not d_inspect)
        s_base = d_arg[0];
    else
    {
        char const *base = getenv("HOME");
        if (base == 0)
            throw Exception{} << "no `HOME' environment variable available";
        
        s_base = base;
    }

    string canon = canonical(s_base, s_ec);
    if (s_ec)
        throw Exception{} << "`--base " << s_base << 
                                            "' must be an absolute path";
    s_base = canon;
}

// static
void Options::setBase(string &path)
{
    if (path.front() == '~')            // replace ~ by s_base
        path.replace(0, 1, s_base);

    else if (path.front() != '/')       // or prefix config dir for plain
        path.insert(0, s_configDir);    // filenames 

    path = absolute(path, s_ec);
    if (not exists(path, s_ec))
        ofstream{path};

    path = canonical(path, s_ec);

    if (s_ec or path.front() != '/')
        throw Exception{} << '`' << path << "' must be an absolute path";
}


