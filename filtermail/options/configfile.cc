#define XERR

#include "options.ih"

void Options::configFile()
{
    string configPath;

    if (d_arg.option(&configPath, 'c') == 0) // config option not used
        configPath = s_configFile;           // use the default

    setBase(configPath);                    // maybe use --base

                                            // determine config file's dir
    (s_configDir = path{ configPath }.parent_path()) += '/';
                                            
                                            // switch to the config file's
    current_path(s_configDir, s_ec);        // dir
    if (s_ec)
        throw Exception{} << "cannot cd to the config file's dir (" <<
                             configPath << ')';

    string configFile = path{configPath}.filename();
    if (not exists(configFile, s_ec))
        throw Exception() << "config file `" << configPath << 
                                                         "' not available";
    d_arg.open(configFile);
}





