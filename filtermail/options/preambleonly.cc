//#define XERR
#include "options.ih"

void Options::preambleOnly() const
{
    if (d_preamble)
        throw "Preamble successfully completed";
}
