//#define XERR
#include "parser.ih"

    // at condTerm its rules have been handled. If d_condition is false the
    // full (and-combined) condition is false, and no further actions are 
    // required. If true, d_condition's value is set to the d_rule match
    // return, updated with 'truth'. If now false then no file switches for
    // subsequent and-terms are needed. At the next 'if'
    // 'startIf' reactivates file switching

void Parser::condTerm()
{
    if (d_onlyParse)                    // earlier term already failed: done
        return;
                                        // no use checking subsequent
                                        // conditions if the current condition
                                        // fails.
    if (not d_filter.matchCondition())      // condition/matched
    {
        d_onlyParse = true;
        d_scanner.stopFileSwitching();  // value is hereafter irrelevant. 
    }
}



