//#define XERR
#include "parser.ih"

    // called when an if-statement and rule was matched.

void Parser::action()
{
    if (d_onlyParse)
        return;
                                                            // all done: stop
    if (d_nErrors_ == 0 and d_filter.accept(d_scanner.action()))    // parsing
        ACCEPT();

    // otherwise continue syntax checking
}
