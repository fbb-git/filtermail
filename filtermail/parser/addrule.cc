//#define XERR
#include "parser.ih"

void Parser::addRule()
{
    d_expect = "yy-mm-dd date specification";

    size_t idx = d_scanner.idx();

    if (idx <= 2)
    {
        d_onlyParse = true;
        d_expect = "rule hit count ending >= col. 3";
        d_received = false;
        error();
    }

    if (not d_onlyParse)                            // .f -> Rules::addRule
        d_filter.addRule(
            d_scanner.nextOffset(),    // Rule's begin offset
            d_scanner.offset(),        // Rule's line offset
            matched(),                 // nr text
            idx
        );
}

