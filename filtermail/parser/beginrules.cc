//#define XERR
#include "parser.ih"

void Parser::beginRules()   // the scanner has already opened the rules file
{
    if (d_onlyParse)
        return;

    string const &filename = d_scanner.filename();

    d_filter.rulesFile(filename);

    d_expect = "<nr> in `"  + filename + '\'';
}
