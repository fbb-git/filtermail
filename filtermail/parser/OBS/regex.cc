#include "parser.ih"

void Parser::regex()
{
    string ret;

    if (not d_filter.ruleOK()
    if (not d_rule)                     // once a rule fails, no further
        return ret;                     // matches are required

    ret = d_scanner.matched();
    ret.pop_back();                     // remove the final quote

    size_t pos = 0;                     // change all \' into '
    while ((pos = ret.find("\\'", pos)) != string::npos)
        ret.erase(pos, 1);

    return ret;
}
