#include "parser.ih"

void Parser::expression(eTruth truth)
{
    
    if (d_rulePtr->available()) // it's already known that this rule is 
        return;                 // matched

    d_rulePtr->expression(truth, regex);

    d_rule = d_ruleFiles.match(truth, regex);

    cout << __FILE__ ": d_rule = " << d_rule << "\n";
}
