#define XERR
#include "parser.ih"

void Parser::expression(eTruth truth) 
{
    if (not d_onlyParse)
        d_filter.matchExpr(truth);
}
