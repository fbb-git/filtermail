//#define XERR
#include "parser.ih"

void Parser::date()
{
    d_expect = "match type [cinps] or scipt";

    if (not d_onlyParse)                        // set the Rule's date idx
        d_filter.setDate(
            d_scanner.matched(), d_scanner.idx() - Scanner::DATE_WIDTH
        );
}
