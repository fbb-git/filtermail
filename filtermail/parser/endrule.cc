//#define XERR
#include "parser.ih"

    // rule completed. Th Rule was added by nr.cc
void Parser::endRule()
{
    if (d_onlyParse)
        return;

    d_filter.endRule(d_scanner.offset());

    d_scanner.setNextOffset();

    if (d_filter.matchRule())               // true: a Rule matched
        d_scanner.toEOF();                  // then skip the remainder 
}
