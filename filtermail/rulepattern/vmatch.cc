//#define XERR
#include "rulepattern.ih"

// overrides
bool RulePattern::vMatch(std::string const &hdr) const
{
    return d_pattern << hdr;
}
