inline bool Rules::ruleMatched() const
{
    return not d_matched.empty();   // d_ruleMatched;
}

inline Rule const &Rules::rule(size_t idx) const
{
    return d_ruleVect[idx];
}

inline size_t Rules::size() const
{
    return d_ruleVect.size();
}
        
inline size_t Rules::idx() const
{
    return d_next - 1;
}
        
