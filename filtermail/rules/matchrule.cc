//#define XERR
#include "rules.ih"

//  Filer::accept updates the rule-line(s) in pattern files.

bool Rules::matchRule()
{
    if (d_rule->matched())
        d_matched.push_back(d_next - 1);

    return ruleMatched();
}
