//#define XERR
#include "rules.ih"

Rule &Rules::addRule(size_t begin, size_t rule, string const &nrTxt,
                     size_t beyondIdx)
{
    if (d_next == d_ruleVect.size())                // a new Rule
        d_ruleVect.push_back(Rule{ begin, rule, nrTxt, beyondIdx });
    else
        d_ruleVect[d_next].reset();

    d_rule = &d_ruleVect[d_next++];

    return *d_rule;
}
