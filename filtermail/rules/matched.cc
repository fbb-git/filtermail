//#define XERR
#include "rules.ih"

SizetVect const &Rules::matched()
{
    if (d_matched.size() > 1)
    {
        sort(d_matched.begin(), d_matched.end());
        d_matched.resize(
            unique(d_matched.begin(), d_matched.end()) - d_matched.begin()
        );
    }
    return d_matched;
}
