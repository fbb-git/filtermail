#ifndef INCLUDED_RULES_
#define INCLUDED_RULES_

#include <iosfwd>
#include <vector>

#include "../rule/rule.h"

// all rules defined by its file. The Rule objects
// define the locations and comment of the separate rules. Once a Rule
// MATCHED matched() returns true

using RuleVect = std::vector<Rule>;

using SizetVect = std::vector<size_t>;

class Rules
{
    SizetVect d_matched;             // indices of matched rule(s)

    RuleVect d_ruleVect;
    size_t d_next;                  // the index of the next Rule
    Rule *d_rule;                   // points to the currently processed Rule

    public:
        Rules();

        Rule &addRule(size_t begin, size_t rule, 
                      std::string const &nrTxt, size_t beyondIdx);

                                        // Rule idx matches
        void details(std::string const &fname, size_t idx);    

        bool ruleMatched() const;                                       // .f

        void reset();

        size_t size() const;                                            // .f

        bool matchRule();               // true once a Rule matches 

        SizetVect const &matched();

        Rule const &rule(size_t idx) const;                             // .f

                                        // the current Rule file's 
        size_t idx() const;             // rule index                   // .f

    private:
        void update(std::ostream &file, size_t idx) const;
        void swapRule(std::fstream &file, size_t idx) const;
};

#include "rules.f"

#endif



