//#define XERR
#include "rules.ih"

Rule *Rules::rule(size_t offset)
{
    auto end = d_offsetRuleVect.end();
    d_iter = find_if(d_iter, end,
                [&](OffsetRule const &item)
                {                           // find the matching offset
                    return offset == item.offset();   
                }
            );

    if (d_iter == end)            // OffsetRule didn't exist yet: make it
    {
        d_offsetRuleVect.push_back({ offset, Rule{} });
        d_iter = d_offsetRuleVect.rbegin().base();
    }

    return &d_iter->rule();
};

