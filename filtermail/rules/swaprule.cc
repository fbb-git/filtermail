//#define XERR
#include "rules.ih"

void Rules::swapRule(fstream &file, size_t idx) const
{
    update(file, idx);                          // update nr/date of rule idx

    string previous(d_ruleVect[idx - 1].width(), 0);
    string current(d_ruleVect[idx].width(), 0);
    size_t begin = d_ruleVect[idx - 1].beginOffset();

    file.seekg(begin);                          // location of the prev. Rule

    file.read(&previous.front(), previous.length());// read the prev. Rule
    file.read(&current.front(), current.length());  // read the current Rule

    file.seekp(begin);                              // reverse the Rules
    file.write(&current.front(), current.length()); // write the current Rule
    file.write(&previous.front(), previous.length());// write the prev. Rule
    
}

