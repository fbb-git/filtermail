#ifndef INCLUDED_STRINGCASE_
#define INCLUDED_STRINGCASE_

#include "../specstring/specstring.h"

class StringCase: public SpecString
{
    public:
        ~StringCase() override;

    private:
        bool vMatch(std::string const &hdr) const override;
};
        
#endif
