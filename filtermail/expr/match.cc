//#define XERR
#include "expr.ih"

bool Expr::match()
{
    SpecBase *matchObjPtr = getMatchObject();

    matchObjPtr->pattern(d_regex);

    if (d_matchMode == SCRIPT)
        return script(*matchObjPtr);
                                                // inspect header (ptrs) in 
                                                // d_headers[headersIdx()]
    for (string const &str: Mail::instance().headers())
    {
        if (matchObjPtr->match(str))        // return true at a match
            return true;
    }

    return false;                               // no match
}
