#ifndef INCLUDED_EXPR_
#define INCLUDED_EXPR_

#include <string>
#include <vector>
#include <memory>

#include "../enums/enums.h"
#include "../specbase/specbase.h"

// Expr syntax:     letter [not] regex

namespace FBB
{
    class OneKey;
    class TempStream;
}

class Expr
{
    size_t d_id;
    static size_t s_id;
    static bool s_interactive;

    bool d_available;               // set to true after regex()

                                    // the handling mode of the regex
    //int d_cinps;                    // (= Expr's letter argument)
    eMatchMode d_matchMode;

    std::string d_regex;

    using UptrSpecBase = std::unique_ptr<SpecBase>;

                                // 0-ptrs or pointers to match objects
                                // derived from SpecBase
    static std::vector<UptrSpecBase> s_matchPtr; 

    static std::unique_ptr<FBB::OneKey> s_oneKey;

    static std::unique_ptr<FBB::TempStream> s_headers;
    static std::unique_ptr<FBB::TempStream> s_input;

    public:
        Expr(eMatchMode matchMode);

        size_t id() const;
        bool regex(std::string const &txt);
        bool matched(eTruth truth);

        static bool interactive();
        static void setInteractive(bool interactive);

        static std::istream &input();

    private:
        bool match();
        SpecBase *getMatchObject();
        bool script(SpecBase &scriptObj);

        static bool update(bool &var, eTruth truth);
};

// static
inline bool Expr::interactive()
{
    return s_interactive;
}

inline size_t Expr::id() const
{
    return d_id;
}

#endif
