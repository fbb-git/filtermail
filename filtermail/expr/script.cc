//#define XERR
#include "expr.ih"

    // Script has already received the regex (via 'match()')
bool Expr::script(SpecBase &matchObj)
{
    if (not s_headers)                          // copy the header/input
    {                                           // to files
        s_headers.reset(new TempStream);
        Mail::instance().copyTo(*s_headers);
        s_input.reset(new TempStream);
        *s_input << cin.rdbuf();
    }

    return matchObj.match(s_headers->fileName() + ' ' + s_input->fileName());
}
