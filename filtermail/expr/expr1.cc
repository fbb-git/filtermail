//#define XERR
#include "expr.ih"

Expr::Expr(eMatchMode mode)
:
    d_id(s_id++),

    d_available(false),
    d_matchMode(mode)
{}
