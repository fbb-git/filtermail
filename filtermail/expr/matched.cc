//#define XERR
#include "expr.ih"

bool Expr::matched(eTruth truth)
{
    bool result;

    if (not s_interactive)
        result = match();               // try to match Expr to the mail
    else
    {
        // perform the test
        cout << __FILE__ "      testing Expr " << d_matchMode << ' ' << 
                d_regex << "\n" "press 'y' for matching: ";
        int ch = (*s_oneKey).get();
        cout << "\n\n";
    
        result = (ch == 'y');
    }

    update(result, truth);
    return result;
}
