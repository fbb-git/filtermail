//#define XERR
#include "expr.ih"

// static
bool Expr::update(bool &var, eTruth truth)
{
    if (truth == REVERSE)
        var = not var;

    return var;
}
