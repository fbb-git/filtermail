//#define XERR
#include "expr.ih"

// static
void Expr::setInteractive(bool interactive)
{
    s_interactive = interactive;

    if (s_interactive)
        s_oneKey.reset(new OneKey{ OneKey::ON });
}
