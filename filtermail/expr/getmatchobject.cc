//#define XERR
#include "expr.ih"

SpecBase *Expr::getMatchObject()
{
    UptrSpecBase &ptr = s_matchPtr[d_matchMode];    // the used match obj. ptr
                      
    if (not ptr.get())                              // not avail.: get it
    {
        switch (d_matchMode)
        {
            case eMatchMode::CIDR:
                ptr = unique_ptr<Cidr>{ 
                                new Cidr{ Options::instance().ip4() } 
                                      };
            break;

            case eMatchMode::STRING:
                ptr = unique_ptr<StringCase>{ new StringCase };
            break;

            case eMatchMode::STRING_NOCASE:
                ptr = unique_ptr<StringNoCase>{ new StringNoCase };
            break;

            case eMatchMode::PATTERN:
                ptr = unique_ptr<RulePattern>{ new RulePattern{ true} };
            break;

            case eMatchMode::PATTERN_NOCASE:
                ptr = unique_ptr<RulePattern>{ new RulePattern{ false } };
            break;

            case eMatchMode::SCRIPT:
                ptr = unique_ptr<Script>{ new Script };
            break;

            default:            // eMatchModeSize is never a match mode
            throw Exception{} << "internal error: " __FILE__;
        }
    }

    return ptr.get();
}
