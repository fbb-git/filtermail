//#define XERR
#include "expr.ih"

// static
istream &Expr::input()
{
    if (not s_input)
        return cin;

    s_input->seekg(0);      // reset the copied input stream
    return *s_input;        // and return it.
}
