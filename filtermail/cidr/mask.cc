#define XERR
#include "cidr.ih"

// static
uint32_t Cidr::mask()
{
    return 
        s_pattern.end() < 7 ?
            std::numeric_limits<uint32_t>::max()
        :
            ~( (1 << uint32_t( 32 - stoul(s_pattern[7]) ) ) - 1 );
}
