inline Cidr::Cidr(std::string const &ip4)
:
    d_ip4(ip4)
{}

inline bool Cidr::matchIP4() const
{
    return d_cidr == (binary(d_ip4) & d_mask);
}

