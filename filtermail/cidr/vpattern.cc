//#define XERR
#include "cidr.ih"

    // pattern is +/-  {\d+\.}{3}\d+/\d+ (see data.cc for specific form)

// overrides
void Cidr::vPattern(std::string const &pattern) 
{
    if (not (s_pattern << pattern))
        throw Exception{} << "Invalid cidr pattern `" << pattern << '\'';

    d_cidr = binary(s_pattern);
    d_mask = mask();
}
