inline bool SpecBase::match(std::string const &hdr) const
{
    return vMatch(hdr);
}

inline void SpecBase::pattern(std::string const &txt)
{
    vPattern(txt);
}
