#define XERR
#include "main.ih"

void preamble()
{
    Options &options = Options::instance();

                                    // interactive avoids reading a mail file
                                    // when options.preamble() is specified
    bool interactive = options.preamble() or options.interactive();

    Expr::setInteractive(interactive);
    Mail::initialize(interactive);

    Log::initialize(options.logPtr());

    Rule::setNow();

    options.preambleOnly();
}







