//#define XERR
#include "scanner.ih"

int Scanner::rulesFile()
try
{
    error_code ec;

    string next = String::trim(matched());

    if (d_pushStreamOK)
    {
        error_code ec;
        d_offset = 0;
        d_nextOffset = 0;
        d_pushed = true;
        pushStream(next);
    }

    return Tokens::FILE;
}
catch (...)
{
    cout << "       " __FILE__ " CANNOT READ `" << matched() << "'\n";
    return ' ';
}
