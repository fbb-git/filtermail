//#define XERR
#include "scanner.ih"

int Scanner::setIdx(int retToken)
{
    d_idx = d_input->line().find_last_not_of(" \t", d_input->idx()) + 1;
    return retToken;
}
