//#define XERR
#include "scanner.ih"

namespace
{

unordered_map<Scanner::StartCondition_, char const *> s_ms =
{
    {Scanner::StartCondition_::regex,   "regex"},
    {Scanner::StartCondition_::err,     "err"},
    {Scanner::StartCondition_::INITIAL, "INITIAL"},
};

}

void Scanner::toMs(StartCondition_ sc)
{
    begin(sc);
}
