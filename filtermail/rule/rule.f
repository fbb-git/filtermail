inline size_t Rule::beginOffset() const
{
    return d_beginOffset;
}

inline std::string const &Rule::date() const
{
    return d_date;
}

inline size_t Rule::endOffset() const
{
    return d_endOffset;
}

inline void Rule::setEnd(size_t offset)
{
    d_endOffset = offset;
}

inline size_t Rule::size() const
{
    return d_exprVect.size();
}

inline size_t Rule::width() const
{
    return d_endOffset - d_beginOffset;
}

