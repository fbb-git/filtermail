//#define XERR
#include "rule.ih"

Expr &Rule::matchMode(eMatchMode mode) 
{
    if (d_next == d_exprVect.size())     // a new Expr
        d_exprVect.push_back(Expr{ mode });

//    else
//        d_exprVect[d_next].reset();

    d_expr = &d_exprVect[d_next++];

    return *d_expr;
}

