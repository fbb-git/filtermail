//#define XERR
#include "rule.ih"

void Rule::writeNr(ostream &out) const
{
    out.seekp(d_ruleOffset + d_beyondIdx - d_nr.length());
    out << d_nr;
}
