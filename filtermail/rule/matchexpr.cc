//#define XERR
#include "rule.ih"

void Rule::matchExpr(eTruth truth)
{
    if (not d_matched)              // done once it's known that the Rule
        return;                     // fails

    if (not d_expr->matched(truth)) // the Expr failed -> the Rule too
        d_matched = false;
}




