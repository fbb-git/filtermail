//#define XERR
#include "filter.ih"

void Filter::setHeader(std::string const &header)
{
    Mail::instance().hdr(header);       // activate the specified header
    d_current.header(header);
}
