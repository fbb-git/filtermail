//#define XERR
#include "filter.ih"

// if a Rule matched perform the action otherwise return false
bool Filter::accept(eAction type)     
{
    if (not d_condition.matched())     // the condition(s) didn't match
        return false;

    // perform the 'action'
    d_action = type;

    for (string const &file: d_matchedFiles)
    {
        Rules &rules = d_map[file];

        for (size_t idx: rules.matched())
            rules.details(file, idx);
    }
      
    d_current.log(type);
  
    return true;
}



