#ifndef INCLUDED_ENUMS_H_
#define INCLUDED_ENUMS_H_

enum eAction    // update options/mailfile.cc and current/log.cc
{               // when changing Action
    ACCEPT,
    IGNORE,
    SPAM,
    eActionSize
};

enum eTruth
{
    USE,
    REVERSE
};

enum eMatchMode                  // matching types (see also ../config/spec)
{
    CIDR,                       // c <- letters used in the rules
    STRING,                     // s
    STRING_NOCASE,              // i
    PATTERN,                    // p
    PATTERN_NOCASE,             // n
    SCRIPT,                     // $

    eMatchModeSize              // recompile expr/data.cc if a mode is *added*
};

#endif
