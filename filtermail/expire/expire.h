#ifndef INCLUDED_EXPIRE_
#define INCLUDED_EXPIRE_

#include <unordered_set>
#include <string>
#include <memory>
#include <fstream>

#include <bobcat/tempstream>

class Options;

class Expire
{
    enum Type
    {
        KEEP,
        EXPIRE,
        DONE                // at EOF
    };

    struct Next
    {
        Type type;          // type of the next entry
        std::streamoff end; // end offset of the next entry
    };

    Options &d_options;
                                            // skipped and already processed
    std::unordered_set<std::string> d_skip; // rule files

    std::string d_expiryDate;

                            // cf: ruleFile
    bool d_first;           // first time #= is seen in rule()
    bool d_comment;         // true if the entry contains comment

    std::ifstream d_current;                    // current rules file
    std::string const *d_fnamePtr;              // and its name

    std::unique_ptr<FBB::TempStream> d_updated; // updated rules file
    std::unique_ptr<std::ofstream> d_expired;   // expired entries file
    
    public:
        Expire();
        bool check();       // true: -e was specified

    private:
        void checkFiles(bool expired);
        Next entry();
        bool expired(std::string const &line);
        void processRules(std::string const &spec);
        void processCurrent();
        void ruleFile(std::string const &fname);
        void write(Type type, size_t begin, size_t end);
};
        
#endif



