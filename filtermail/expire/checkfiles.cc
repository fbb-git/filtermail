//#define XERR
#include "expire.ih"

void Expire::checkFiles(bool expired)
{
    if (not expired or d_expired)       // no need for extra files or they
        return;                         // already exist

                                        // tmp. file to receive kept entries
    d_updated.reset(new TempStream{ *d_fnamePtr });

    d_expired.reset(                    // filename to receive expired entries
        new ofstream{ 
                Exception::factory<ofstream>(
                *d_fnamePtr + ".exp", ios::in | ios::ate, ios::out
                )
            }
    );
}
