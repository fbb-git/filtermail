//#define XERR
#include "expire.ih"

void Expire::write(Type type, size_t begin, size_t end)
{
    if (not d_expired)              // no updates if no expired entries
        return;

    size_t nChars = end - begin;
    unique_ptr<char[]> buffer{ new char[nChars] };

    d_current.seekg(begin);
    d_current.read(buffer.get(), nChars);

    if (type == EXPIRE)
    {
        d_expired.get()->write(buffer.get(), nChars);
        d_expired.get()->flush();
    }
    else
    {
        d_updated.get()->write(buffer.get(), nChars);
        d_updated.get()->flush();
    }
}
