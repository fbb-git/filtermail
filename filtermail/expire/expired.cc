//#define XERR
#include "expire.ih"

bool Expire::expired(string const &line)
{
    istringstream in{ line };
    string date;

    in >> date >> date;                     // get the entry's date 

    bool expired = date < d_expiryDate;     // entry expired

                                            // if not yet available:
                                            // create files to receive
    checkFiles(expired);                    // expired/updated entries

    return expired;
}
