//#define XERR
#include "current.ih"

void Current::reset(size_t rulesLine)
{
    d_rulesLine = rulesLine;
    d_data.clear();
}
