#ifndef INCLUDED_CURRENT_
#define INCLUDED_CURRENT_

#include <vector>
#include <string>

#include "../enums/enums.h"

class Current
{
    struct Data
    {
        std::string header;
        size_t idx;
        std::string filename;
    };

    size_t d_rulesLine = std::string::npos;
    std::vector<Data> d_data;

    public:
        void reset(size_t rulesLine);
        void header(std::string const &hdrName);
        void ruleFile(std::string const &filename);
        void matchedRule(size_t idx);
        void log(eAction type);
};
        
#endif
