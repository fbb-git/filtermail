//#define XERR
#include "inspector.ih"

bool Inspector::privateIP4() const
{
    return
        d_ip.find("10.") == 0       or
        d_ip.find("127.") == 0      or
        d_ip.find("192.168.") == 0  or 
        ip172_16();
}
