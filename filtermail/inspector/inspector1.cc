//#define XERR
#include "inspector.ih"

Inspector::Inspector()
:
    d_proc( "tput clear", Proc::COUT | Proc::IGNORE_CERR, Proc::USE_PATH )
{
    if (Options::instance().cls())
    {
        d_proc.start();
        d_proc.finish();
    }

    receivedHeaders();
}
