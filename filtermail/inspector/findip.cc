//#define XERR
#include "inspector.ih"

bool Inspector::findIP(string const &hdr)
{
    if (s_cidr << hdr)
    {
        d_ipType = IPv4;
        d_ip = s_cidr[1];
        if (privateIP4())                                // private ranges
            return false;
    }
    else if (s_cidr6 << hdr)
    {
        d_ipType = IPv6;
        d_ip = s_cidr6[1];
        if (privateIP6())                   // private IP6 range
            return false;
    }
    else
    {
        d_ipType = NONE;
        return false;
    }

    cout << hdr.substr(sizeof("Received:"), 
                       hdr.find_first_of('\n') - sizeof("Received:")) << "\n"
            "IP = `" << d_ip << "', ";
    return true;
}
