//#define XERR
#include "mail.ih"

void Mail::hdr(std::string header)
{
                                            // mail headers not used
    if (Expr::interactive() or d_mh.size() == 0)
        return;
                                            // if the headers were requested
                                            // earlier then use those
    if (auto iter = d_usedHdrs.find(header); iter != d_usedHdrs.end())
    {
        d_hdrsIdx = iter->second;
        return;
    }

    d_hdrsIdx = d_headers.size();           // idx of the new series of hdrs

    d_usedHdrs[header] = d_hdrsIdx;

    d_headers.resize(d_hdrsIdx + 1);        // room for the new headers

                                            // at 'hdr+' or plain 'hdr'
    MailHeaders::Match matchType =          // use the initial search mode
                            header.back() == '+' or header.back() != ':'? 
                                MailHeaders::INITIAL 
                            : 
                                MailHeaders::FULL;

                                            // a plain header w/o + or :
    bool plain = "+:"s.find(header.back()) == string::npos;

    if (not plain)
        header.pop_back();
                                            // select the matching headers
    d_mh.setHeaderIterator(header.c_str(), matchType);

    VectStr &dest = d_headers.back();       // add the matched headers
    if (plain)
        dest.push_back(rmHeader(*d_mh.beginh()));
    else
    {
        for (auto const &hdr: Ranger{ d_mh.beginh(), d_mh.endh() })
            dest.push_back(rmHeader(hdr));
    }
}



