#ifndef INCLUDED_MAIL_
#define INCLUDED_MAIL_

#include <iosfwd>
#include <string>
#include <vector>
#include <unordered_map>
#include <memory>

#include <bobcat/mailheaders> 

#include "../enums/enums.h"

class Mail
{
    friend std::ostream &operator<<(std::ostream &out, Mail const &mail);

    static std::unique_ptr<Mail> s_mail;

                                // index in d_headers accessing the requested
    size_t d_hdrsIdx;           // headers

    FBB::MailHeaders d_mh;      // the mail headers

    using MapStrSize_t  = std::unordered_map<std::string, size_t>;

                                // indices of used headers to the elements of
    MapStrSize_t d_usedHdrs;    // d_headers containing their strings


    using VectStr = std::vector<std::string>;
    using VectVectStr   = std::vector<VectStr>;

                                // headers[idx] are the headers corresponding
    VectVectStr d_headers;      // to d_usedHdrs[HdrName]

    public:
                                                        // Mail is a Singleton
        static void initialize(bool interactive);       // false: -> use cin
        static Mail &instance();

        void copyTo(std::ostream &out);

        void hdr(std::string header);                   // use 'header'

        VectStr const &headers() const;                             //  .f

    private:
        Mail();
        std::ostream &insert(std::ostream &out) const;

        static std::string rmHeader(std::string const &header);
};

#include "mail.f"

#endif


