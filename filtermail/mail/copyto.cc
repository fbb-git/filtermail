//#define XERR
#include "mail.ih"

void Mail::copyTo(ostream &out)
{
    copy(d_mh.begin(), d_mh.end(), 
            ostream_iterator<string const>(out, "\n"));

    out.flush();
}
