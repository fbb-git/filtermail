#include "main.ih"

int handleException()
try
{
    rethrow_exception(current_exception());
} 
catch (int ret)                 // handle the known exceptions
{
    return ArgConfig::instance().option("hv") ? 0 : ret;
}
catch (exception const &exc)
{
    cerr << "Error: " << exc.what() << '\n';
    return 1;
}
catch (char const *msg)
{
    cout << msg << '\n';
    return 0;
}
catch (...)                     // and handle an unexpected exception
{
    cerr << "unexpected exception\n";
    return 1;
}
