#ifndef INCLUDED_SCRIPT_
#define INCLUDED_SCRIPT_

#include "../specbase/specbase.h"

class Script: public SpecBase
{
    int d_mode;             // set by vPattern, a Proc::ProcType value
    std::string d_command;

    public:
        // default: Script();
        ~Script() override;

    private:
        bool vMatch(std::string const &tmpStreamNames) const override;
        void vPattern(std::string const &scriptSpec) override;
};
        
#endif
