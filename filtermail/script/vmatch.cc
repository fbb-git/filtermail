//#define XERR
#include "script.ih"

// First name are the mail headers, 2nd name is the mail input 
// (cf. Expr::script)

// overrides
bool Script::vMatch(string const &tmpStreamNames) const
{
    string cmd{ d_command.substr(0, d_command.find_first_of(" \t")) };

    error_code ec;

    if (not exists(cmd, ec))
        throw Exception{} << cmd << " DOES NOT EXIST";

    file_status stat = status(cmd, ec);
    
    if (
        (status(cmd, ec).permissions() & perms::owner_exec) 
        != perms::owner_exec
    )
        throw Exception{} << cmd << " NOT EXECUTABLE";
    
    cmd = d_command + ' ' + tmpStreamNames;

    Proc proc{ cmd, Proc::NONE, static_cast<Proc::ProcType>(d_mode) };

    proc.start();
    int ret = proc.waitForChild();

    return ret == 0;
}



