#ifndef INCLUDED_SPECSTRING_
#define INCLUDED_SPECSTRING_

#include <string>
#include "../specbase/specbase.h"

class SpecString: public SpecBase
{
    std::string d_pattern;

    protected:
        ~SpecString() override;

        std::string const &pattern() const;                             // .f
        void setPattern(std::string const &pattern);                    // .f

    private:
        void vPattern(std::string const &hdr) override;
};

#include "specstring.f"

#endif
