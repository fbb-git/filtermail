//#define XERR
#include "inspectcidr.ih"

// static
size_t InspectCidr::binary(string const &address)
{
    istringstream in(address);
    size_t ret = 0;

    for (size_t octet = 0; octet != 4; ++octet)
    {
        size_t byte;
        in >> byte;                     // extract the next octet value
        (ret <<= 8) += byte;            // move the existing value 8 bits up
        in.seekg(1, ios::cur);          // skip the dot (fails at octet 3: OK)
    }
    return ret;
}
