//                     usage.cc

#include "main.ih"

namespace {
char const info[] = R"_( [options] base
Where:
    [options] - optional arguments (short options between parentheses):
        --accept [:HDRS:]path - path receiving accept-marked mail. If not
                                specified accept-marked mail is ignored (not
                                saved on file). Prepend :HDRS: to `path' to
                                merely write the received mail's From: and
                                Subject: headers to `path'.
        --cls <what>          - specify 'yes' (default) to clear the screen
                                before inspecting the e-mail. Otherwise 
                                (e.g., 'no') the screen is not cleared. This
                                option is only used when the --inspect option
                                is specified.
                                
        --config (-c) path    - the path to the configuration file.
                                (default: ~/.filtermail/config)
        --expire (-e) date    - inspect rule files for expired rules (no mail
                                 is read). Use format yy-mm-dd when specifying
                                 'date'. Rules having older date stamps are
                                 removed from their files and are stored in
                                 files having extension .exp
        --help (-h)            - provide this help
        --ignore [:HDRS:]path  - path receiving ignore-marked mail. If not
                                 specified ignore-marked mail is ignored. 
                                 Prepend :HDRS: to `path' to merely write the
                                 received mail's From: and Subject: headers to
                                 `path'.
        --inspect (-I)         - inspect the 'Received:' headers (see below).
        --interactive (-i)     - specified rules are interactively simulated 
                                 (no mail is read)
        --log (-l) spec        - specify the log-facility. By default logging
                                 is not used (see the default config file)
        --preamble             - verify that the basic options are correctly
                                 specified.
        --received (-R) spec   - if a Received: header contains 'spec' then
                                 only subsequent Received: headers are
                                 inspected. If not specified or if no
                                 Received: header contains 'spec' then all
                                 Received: headers are inspected. This option
                                 is only used when the --inspect option is
                                 specified.
        --rules (-r) path      - rules specification file 
                                 (default: ~/etc/mailfilter/rules)
        --spam [:HDRS:]path    - path receiving spam-marked mail. If not
                                 specified spam-marked mail is ignored.
                                 Prepend :HDRS: to `path' to merely write the
                                 received mail's From: and Subject: headers to
                                 `path'.
        --syntax               - only perform a syntax check of the filter 
                                 rules
        --version (-v)         - show version information and terminate
    
    base - absolute path to the user's home directory. When specified
                              other path specifications may start with
                              ~/, replacing ~ by --base's path

    Unless --expire or --interactive is specified mail is read from stdin. 
    
    When the --inspect option is specified the 'Received:' headers found in
    the e-mail are inspected. IP addresses found in those headers are passed
    to 'whois'. The header's content, the inspected IP address, its country
    and cidr-range are written to the std. output stream.

)_";

}

void usage(std::string const &progname)
{
    cout << "\n" <<
    progname << " by " << Icmbuild::author << "\n" <<
    progname << " V" << Icmbuild::version << " " << Icmbuild::years << "\n"
    "\n"
    "Usage: " << progname << info;
}
