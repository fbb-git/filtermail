#ifndef INCLUDED_STRINGNOCASE_
#define INCLUDED_STRINGNOCASE_

#include <string>
#include "../specstring/specstring.h"

class StringNoCase: public SpecString
{
    std::string d_first;

    public:
        ~StringNoCase() override;

    private:
        bool vMatch(std::string const &hdr) const override;
        void vPattern(std::string const &pattern) override;

};
        
#endif
