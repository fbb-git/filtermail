#include "uinput.ih"

void UInput::reRead(size_t ch)
{
    --d_lineOffset;

    if (ch < 0x100)
    {
        if (ch == '\n')
            --d_lineNr;

        d_deque.push_front(ch);
    }
}
