#ifndef INCLUDED_UINPUT_H_
#define INCLUDED_UINPUT_H_

#include <iostream>
#include <deque>
#include <queue>
#include <string>

class UInput
{
    std::istream *d_in;                 // ptr for easy streamswitching

    std::size_t d_lineOffset;
    std::size_t d_idx;
    size_t d_lineNr;                    // line count
    size_t d_reread;

    std::string d_line;

    std::queue<size_t> d_queue;         // nl queue
    std::deque<unsigned char> d_deque;  // pending input chars

    size_t d_front;

    public:
                                   // iStream: dynamically allocated
        UInput(std::istream *iStream, size_t lineNr = 1);
        size_t get();                   // the next range


        void reRead(size_t ch);         // push back 'ch' (if < 0x100)
                                        // push back str from idx 'fmIdx'
        void reRead(std::string const &str, size_t fmIdx);
        size_t idx() const;
        size_t lineNr() const;
        size_t lineOffset();
        std::string const &line() const;
        size_t nPending() const;
        void setPending(size_t size);
        void close();                   // force closing the stream
        void toEOF();                   // move the stream to its EOF pos.

    private:
        size_t next();

};

#include "uinput.f"


#endif
