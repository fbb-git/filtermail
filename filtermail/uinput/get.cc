#include "uinput.ih"

size_t UInput::get()
{
    ++d_lineOffset;

    switch (size_t ch = next())         // get the next input char
    {
        case '\n':
            ++d_lineNr;
        [[fallthrough]];

        default:
        return ch;
    }
}

