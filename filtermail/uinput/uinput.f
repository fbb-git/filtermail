// inline void UInput::inc(size_t value)
// {
//     d_lineOffset += value;
// }

inline std::string const &UInput::line() const
{
    return d_line;
}

inline size_t UInput::idx() const
{
    return d_idx;
}

inline size_t UInput::lineNr() const
{
    return d_lineNr;
}

inline size_t UInput::nPending() const
{
    return d_deque.size();
}

inline void UInput::setPending(size_t size)
{
    d_deque.erase(d_deque.begin(), d_deque.end() - size);
}

//inline void UInput::zero()
//{
//    d_lineOffset = 0;
//}
//
