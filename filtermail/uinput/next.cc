#include "uinput.ih"

size_t UInput::next()
{
    size_t ch;

    if (d_deque.empty())                // deque empty: next char fm d_in
    {
        if (d_in == 0 or d_idx == string::npos)
            return AT_EOF;

        if (d_idx != d_line.length())
            ch = d_line[d_idx++];
        else
        {
            ch = '\n';
            d_idx = 
                static_cast<bool>(getline(*d_in, d_line)) ? 0 : string::npos;
        }

        return *d_in ? ch : static_cast<size_t>(AT_EOF);
    }

    ch = d_deque.front();

    d_deque.pop_front();

    return ch;
}
