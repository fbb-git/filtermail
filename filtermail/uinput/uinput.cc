#include "uinput.ih"

UInput::UInput(std::istream *iStream, size_t lineNr)
:
    d_in(iStream),
    d_lineOffset(d_in->tellg()),
    d_idx(0),
    d_lineNr(lineNr),
    d_reread(0),
    d_front(0)
{
    d_idx = static_cast<bool>(getline(*d_in, d_line)) ? 0 : string::npos;
}
