//#define XERR
#include "log.ih"

Log &operator<<(Log &log, std::ios_base &(*func)(std::ios_base &))
{
    if (log.d_outPtr)
        (*func)(*log.d_outPtr);
    return log;
}
        
