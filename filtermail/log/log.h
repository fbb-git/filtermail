#ifndef INCLUDED_LOG_
#define INCLUDED_LOG_

#include <iosfwd>
//#include <fstream>
#include <memory>

namespace FBB
{
    class SyslogBuf;
    class LogBuf;
}

class Log
{
    template <typename Type>
    friend Log &operator<<(Log &log, Type const &type);                 // .f

                                                            // opinsert1.cc
    friend Log &operator<<(Log &log, std::ios_base &(*func)(std::ios_base &));

                                                            // opinsert2.cc
    friend Log &operator<<(Log &log, std::ostream &(*func)(std::ostream &));

    std::ostream *d_outPtr;

    static std::unique_ptr<Log> s_logPtr;   // the singleton Log object

    public:
        static void initialize(std::ostream *outPtr);
        static Log &instance();
        
    private:
        Log(std::ostream *outPtr);
};

#include "log.f"

#endif

