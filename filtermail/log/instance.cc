//#define XERR
#include "log.ih"

// static
Log &Log::instance()
{
    if (not s_logPtr)
        throw Exception{} << "Log object not initialized";

    return *s_logPtr;
}
