Filtermail is a program to filter incoming e-mail sending it to the standard mailbox, to a spam-box or to an ignore-box. It's intended to be used in an operating system allowing the use of a 'forward' file specifying a program receiving the incoming e-mail, and deciding how the e-mail should be treated. E.g., in Linux systems users can usually define a ~/.forward file performing this task.

It is possible to completely ignore (i.e., not store) the received mail, but it is advised to do that only after a testing time-interval which is used to gain confidence in the specified filter-rules

Mail is primarily filtered based on the contents of mail headers, but it's also possible to use a script or program to inspect the mail's actual content.

Mail is filtered using rules of the form 
        if Hdr ./rule-file [and Hdr ./rule-file]* ACTION
with rule files containing patterns which are used to check whether the headers (Hdr) are matched by those patterns. Patterns use the following format:
        <hitcount> <date> Mode [not] 'regex' [and Mode [not] 'regex']*
where hitcount (initially 1) is incremented at every hit; date is the yy-mm-dd date-stamp of the most recent hit, [not] is an optional 'not' operator negating the result of matching a header against regex. The ACTION specification is one of accept, ignore or spam.

Refer to the filtermail(1) man-page for more extensive documentation.
